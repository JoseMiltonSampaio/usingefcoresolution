﻿using BusinessLogic.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Linq;

namespace UI.Controllers
{
    public class BlogController : Controller
    {
        private readonly BlogContext _blogContext;

        public BlogController(BlogContext context)
        {
            _blogContext = context;
        }

        public IActionResult Index()
        {
            var blog = _blogContext.Blogs.First();
            return View(blog);
        }
    }
}

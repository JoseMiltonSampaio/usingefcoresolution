﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace BusinessLogic.Models
{
    public class BlogRepository : IBlogRepository
    {
        private readonly BlogContext _context;

        public BlogRepository(BlogContext context)
        {
            _context = context;
        }

        public void RollbackChanges()
        {
            throw new NotImplementedException();
        }

        public void SaveChanges()
        {
            throw new NotImplementedException();
        }

        public IQueryable<Blog> Set()
        {
            return _context.Set<Blog>();
        }

        public IQueryable<T> Set<T>() where T : class
        {
            throw new NotImplementedException();
        }
    }
}
